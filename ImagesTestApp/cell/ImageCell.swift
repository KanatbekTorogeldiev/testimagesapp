//
//  ImageCell.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 14/1/22.
//

import UIKit
import Kingfisher

class ImageCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var authorLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupUI(_ data: ResponseImageModel) {

        DispatchQueue.main.async {
            let url = Foundation.URL(string: data.download_url ?? "")
            let processor = DownsamplingImageProcessor(size: self.imgView.bounds.size)
            self.imgView.kf.indicatorType = .activity
            self.imgView.kf.setImage(with: url, placeholder: nil, options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.7)),
                .loadDiskFileSynchronously],
                progressBlock: .none,
                completionHandler: { result in /*self.hideSkeleton(v: imageView)*/})
        }
        
        authorLbl.text = data.author ?? ""
    }
}
