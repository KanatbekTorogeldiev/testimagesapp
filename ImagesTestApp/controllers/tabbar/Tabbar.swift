//
//  Tabbar.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import UIKit

class Tabbar: UITabBarController, Storyboarded {
    
    var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
    }
    
    func setupTabBar() {
        let mainVC = MainVC.instantiate()
        mainVC.coordinator = coordinator
        mainVC.viewModel = MainViewModel(vc: mainVC)
        let randomVC = RandomVC.instantiate()
        randomVC.coordinator = coordinator
        
        let nv1 = makeNVForTabbar(mainVC, "list.bullet.below.rectangle", "main", "Главная")
        let nv2 = makeNVForTabbar(randomVC, "photo.fill", "photo", "Рандом")
        
        viewControllers = [nv1, nv2]
    
//        for vc in viewControllers ?? [] {
//            if let vc = vc as? UINavigationController{ vc.navigationBar.barStyle = .black; vc.navigationBar.shadowImage = UIImage()}
//        }
        
        self.viewDidLayoutSubviews()
    }
    
    func makeNVForTabbar(_ vc: UIViewController,_ imgName: String,_ accessibilityIdentifier: String?,_ title: String?) -> UINavigationController {
        let nv = UINavigationController()
        nv.viewControllers = [vc]
        nv.tabBarItem.image = UIImage.init(systemName: imgName)
        nv.tabBarItem.selectedImage = tabBarItem.selectedImage?.withRenderingMode(.alwaysOriginal)
        if accessibilityIdentifier != nil {
            nv.tabBarItem.isAccessibilityElement = true
            nv.tabBarItem.accessibilityIdentifier = accessibilityIdentifier ?? nil
        }
        nv.title = title
        return nv
    }
}
