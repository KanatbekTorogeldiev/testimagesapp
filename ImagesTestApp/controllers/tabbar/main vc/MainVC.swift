//
//  MainVC.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import UIKit
import Agrume
import Kingfisher

class MainVC: UIViewController, Storyboarded {

    @IBOutlet weak var tv: UITableView!
    
    var coordinator: MainCoordinator?
    var viewModel: MainViewModel?
    
    let refreshControl = UIRefreshControl()
    let cellId = "ImageCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTV()
        if viewModel == nil { viewModel = MainViewModel(vc: self) }
        viewModel?.listDidChange = { [unowned self] viewModel in
            tv.reloadData()
            if viewModel.list.count <= 100 { animList() }
        }
        
        if viewModel?.getRealmData() == nil { viewModel?.getImages() }
        
    }

    func registerTV() {
        self.title = "Список"
        tv.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tv.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
//        tv.delegate = self; tv.dataSource = self
        
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        viewModel?.page = 1
        viewModel?.getImages()
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ImageCell
        
        if let data = viewModel?.list[indexPath.item] {
            cell.setupUI(data)
        }
        
        if viewModel?.unloadYet ?? false && (viewModel?.list.count ?? 0) - 20 == indexPath.item {
            viewModel?.getImages()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let data = viewModel?.list[indexPath.item] {
            let agrume = Agrume(url: URL(string: data.download_url ?? "")!, background: .blurred(UIBlurEffect.Style.light), dismissal: .withPan(.standard), overlayView: .none)
            agrume.show(from: self)
        }
    }
    
    func animList() {
        tv.reloadData()
        tv.performBatchUpdates({
            UIView.animate(views: tv.visibleCells, animations: (viewModel?.getAnimationType())!, options: .curveEaseInOut)
        }, completion: nil)
    }
}

