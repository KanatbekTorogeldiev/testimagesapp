//
//  MainViewModel.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 14/1/22.
//

import Foundation
import UIKit
import RappleProgressHUD
import RxSwift
import Alamofire
import ViewAnimator
import RealmSwift

protocol MainViewModelDelegate: AnyObject {
    
    init(vc: UIViewController)
    
    var page: Int { get set }
    var unloadYet: Bool { get set }
    
    var list: [ResponseImageModel] { get }
    var listDidChange: ((MainViewModelDelegate) -> ())? { get set }
    
    func getImages()
}

class MainViewModel: MainViewModelDelegate {
    
    let disposeBag = DisposeBag()
    let realm = try! Realm()
    
    var vc: UIViewController? = nil
    var page: Int = 1
    var unloadYet: Bool = true
    var list: [ResponseImageModel] = [] { didSet { self.listDidChange?(self) } }
    var listDidChange: ((MainViewModelDelegate) -> ())?
    
    required init(vc: UIViewController) { self.vc = vc }
    
    func showLoader() {
        let attributes = RappleActivityIndicatorView.attribute(style: RappleStyle.circle, tintColor: .lightGray, screenBG: .clear, progressBG: .clear, progressBarBG: .clear, progreeBarFill: .clear, thickness: 4)
        RappleActivityIndicatorView.startAnimating(attributes: attributes)
    }
    
    func hideLoader() { RappleActivityIndicatorView.stopAnimation() }
}

//MARK: - GETTER & SETTER
extension MainViewModel {
    
    ///# SETTER
    func getAnimationType() -> [AnimationType] {
        return [AnimationType.zoom(scale: 0.5)]
    }
    
    func getRealmData() -> [ResponseImageModel]? {
        let datas = realm.objects(ImagesRealmModel.self)
        var list: [ResponseImageModel] = []
        for data in datas {
            let model = ResponseImageModel(
                id: data.id,
                author: data.author,
                width: data.width,
                height: data.height,
                url: data.url,
                download_url: data.download_url)
            list.append(model)
        }
        self.list = list
        return list.count != 0 ? list : nil
    }
    
    ///# SETTER
    func setDataToDB(_ list: [ResponseImageModel]) {
        if page == 1 {
            realm.beginWrite()
            realm.deleteAll()
            try! realm.commitWrite()
        }
        
        if list.count != 0 {
            realm.beginWrite()
            for obj in list {
                let newObj = ImagesRealmModel()
                newObj.id = obj.id ?? ""
                newObj.author = obj.author ?? ""
                newObj.width = obj.width ?? 0
                newObj.height = obj.height ?? 0
                newObj.url = obj.url ?? ""
                newObj.download_url = obj.download_url ?? ""
                self.realm.create(ImagesRealmModel.self, value: newObj)
            }
            try! realm.commitWrite()
            getRealmData()
        }
    }
}

//MARK: - REQUEST
extension MainViewModel {
    func getImages() {
        let params: [String : Any] = ["page": page, "limit": 100]
        showLoader()
        ApiManager.shared
            .getImages(params)
            .subscribe(
                onNext: { [self] response in
                    hideLoader()
                    if response.count > 0 {
                        if page == 1 { list.removeAll() }
                        setDataToDB(response)
                        self.page += 1
                    } else { unloadYet = false }
                }, onError: { error in
                        self.hideLoader()
    //                    HelperClass.shared.alert("Ошибка", "Сервер не отвечает или проблены с доступом на интернет, попробуйте позднее.", vc: self.vc!)
                    })
//            .disposed(by: disposeBag)
        
    }
}
