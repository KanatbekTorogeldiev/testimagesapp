//
//  RandomVC.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 14/1/22.
//

import UIKit
import Kingfisher

class RandomVC: UIViewController, Storyboarded {

    @IBOutlet weak var imgV: UIImageView!
    
    var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Рандом"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadImage()
    }
    
    func loadImage() {
        DispatchQueue.main.async {
            let date = Date().timeIntervalSince1970
            let url = Foundation.URL(string: "https://picsum.photos/200/300#\(date)")
            let processor = DownsamplingImageProcessor(size: self.imgV.bounds.size)
            self.imgV.kf.indicatorType = .activity
            self.imgV.kf.setImage(with: url, placeholder: nil, options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.7)),
                .loadDiskFileSynchronously],
                progressBlock: .none,
                completionHandler: { result in /*self.hideSkeleton(v: imageView)*/})
        }
    }
}
