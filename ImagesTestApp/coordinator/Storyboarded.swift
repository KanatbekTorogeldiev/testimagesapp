//
//  Storyboarded.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import Foundation
import UIKit

protocol Storyboarded { static func instantiate() -> Self }
extension Storyboarded where Self: UIViewController {
    
    static func instantiate() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T { return T.init(nibName: String(describing: T.self), bundle: nil) }
        return instantiateFromNib()
    }
    
}
