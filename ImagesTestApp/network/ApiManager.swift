//
//  ApiManager.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import Foundation
import RxSwift
import UIKit
import Alamofire
import ObjectMapper
import SwiftUI

///#Api Manager
open class ApiManager: BaseService{
    
    ///#init
    init(url:String) { serverUrl = url }
    
    ///#Singleton
    public static func setup(url:String) { shared = ApiManager(url: url) }
    public static var shared: ApiManager!
    private let serverUrl: String
    
    ///#Header for make request without access token
    private var headers: HTTPHeaders { return ["Content-Type":"application/json"]}
 
    /**
        Recived image's
        - Parameters : limit = 100 & page
        - Returns : array of image url
     */
    public func getImages(_ params: [String:Any]) -> Observable<[ResponseImageModel]> {
        let URL = "\(serverUrl)v2/list"
        
        return self.createRequest(
            url: URL,
            parameters: params,
            headers: self.headers,
            method: .get
        )
    }
}
