//
//  BaseService.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import RxSwift
import ObjectMapper
import Alamofire
import RxAlamofire

/// Base service for API
open class BaseService: SessionDelegate {
    
    private var managerApi : Alamofire.Session
    
    init() { managerApi = Alamofire.Session() }
    
    ///SSL pinning check for  only trust the valid or pre-defined certificate
    private let sessionTrust: Session = {
        let manager = ServerTrustManager(
            evaluators: ["picsum.photos": DisabledTrustEvaluator() ])
        let configuration = URLSessionConfiguration.af.default
        return Session(configuration: configuration, serverTrustManager: manager)
    }()
    
    ///Create base request
    func createRequest<T: Codable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<T>{
        return sessionTrust.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .rx
            .codable()
            .logBeforeRequest{
                Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
            }
    }
    
    /// Cancel  request
    func cancelAllRequests() {
        print("cancelling NetworkHelper requests")
        managerApi.session.invalidateAndCancel()
    }
}

struct Certificates {
    
    private static func certificate(filename: String) -> SecCertificate {
        let filePath = Bundle.main.path(forResource: filename, ofType: "der")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: filePath))
        let certificate = SecCertificateCreateWithData(nil, data as CFData)!
        return certificate
    }
    
}
