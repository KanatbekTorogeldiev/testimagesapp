//
//  Logger.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import Alamofire

/// Helper for API
public class Logger {
    
    //MARK: lifecycle
    public static func logViewDidLoad(_ object:Any){
        let name = String(describing: type(of: object))
        logUrl("vc load   :\(name)")
    }
    
    public static func logDeinit(_ object:Any){
        let name = String(describing: type(of: object))
        logUrl("deinit    :\(name)")
    }
    
    //MARK: networking
    static func logRequest( url:String,
                            parameters: [String: Any]?,
                            headers: HTTPHeaders?,
                            method: HTTPMethod = .get) {
        var logText =   "\n➡️➡️➡️➡️➡️➡️ REQUEST ➡️➡️➡️➡️➡️➡️➡️"
        logText +=      "\n" + "URL          - %@ "
        #if DEBUG
        logText +=      "\n" + "parameters   - \(String(describing: parameters)) "
        logText +=      "\n" + "headers      - \(String(describing: headers)) "
        #endif
        logText +=      "\n" + "method       - \(method) "
        logText +=      "\n➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️"
        logUrl(logText,url:url)
    }
    
    static func logResponse<success, error>(packedResponse:DataResponse<success, error>) {
        #if DEBUG
        var logText =   "\n⬅️⬅️⬅️⬅️⬅️⬅️ RESPONSE  ⬅️⬅️⬅️⬅️⬅️⬅️"
            logText +=      "\n" + "URL          - \(packedResponse.request?.url?.absoluteString ?? "nil")"
        if let statusCode = packedResponse.response?.statusCode{
            logText +=      "\n" + "code         - \(statusCode)"
        }
        if let error = packedResponse.error?.localizedDescription{
            logText +=      "\n" + "error        - \(error)🛑"
        }

        if verbose,
           let data = packedResponse.data,
           let body = String(data: data, encoding: String.Encoding.utf8),
           let headerDate = packedResponse.response?.allHeaderFields["Date"] as? String {
            logText +=      "\n" + "body         - \(body)"
            logText +=      "\n" + "serverDate   - \(headerDate)⏰"
        }
        
        logText +=      "\n⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️⬅️"
        logUrl(logText)
        #endif
    }
    
    //MARK: other
    public static func log(_ text:String,_ logLevel:LogLevel = .none){
        logUrl("\(logLevel.rawValue) \(text)")
    }
    
    //MARK: private
    private static func logUrl(_ logText:String,url:String? = nil){
        #if DEBUG
        print(logText.replacingOccurrences(of: "%@", with: (url ?? "")))
        #else
        logHandler?(logText, url ?? "")
        #endif
    }
    
    private static var verbose = true
    private static var logHandler:((String,String)->())?
    public static func initialize(verbose:Bool,logHandler:@escaping (String,String)->()){
        self.verbose = verbose
        self.logHandler = logHandler
    }
    
    public enum LogLevel:String{
        case
        none    = "",
        info    = "✅",
        warn    = "⚠️",
        err     = "🔴"
    }
}
