//
//  RxAlamofireExtentions.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import Alamofire
import RxSwift
import ObjectMapper
import RxAlamofire

extension Reactive where Base: DataRequest {
    
    public func codable<T:Codable>(decoder:JSONDecoder = JSONDecoder()) -> Observable<T>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<T> in
                return Observable.create{ observer in
                    if T.self == String.self{
                        observer.onNext(response as! T)
                    } else{
                        let res = response.isBlank ? "{}" : response
                        if
                            let jsonData = res.data(using: .utf8),
                            let result = try? decoder.decode(T.self, from: jsonData){
                            observer.onNext(result)
                        } else {
                            observer.onError(RxAlamofireError<Any>(localizedDescription: "JSON_PARSE_ERROR"))
                        }
                    }
                    return Disposables.create()
                }
        }
    }
}

extension Observable {
    
    public func logBeforeRequest(logHandler: @escaping()->()) -> Observable<Element>{
        let observable = self
        return RxSwift.Observable
            .just(Void())
            .flatMap{ _ -> Observable<Element> in
                logHandler()
                return observable
            }
    }
}

fileprivate let FailureResponseBodyErrorKey = "FailureResponseBodyErrorKey", FailureStatusCodeErrorKey = "FailureStatusCodeErrorKey"

fileprivate extension Error {
    var responseBody:String? { return ((self as Any) as? NSError)?.userInfo[FailureResponseBodyErrorKey] as? String }
    var localizedFailureReason:String? { return ((self as Any) as? NSError)?.localizedFailureReason }
    var statusCode:NSInteger? { return ((self as Any) as? NSError)?.userInfo[FailureStatusCodeErrorKey] as? NSInteger }
}

fileprivate func getError() -> NSError{
    let
    userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR"],
    error = NSError(domain: "com.google", code: 1, userInfo: userInfo)
    return error
}


fileprivate extension Reactive where Base: DataRequest {
    func stringWithBodyInError(encoding: String.Encoding? = nil) -> Observable<String> {
        return resultWithBodyInError(responseSerializer: StringResponseSerializer(encoding: encoding))
    }
    
    /**
     Transform a request into an observable of the serialized object.
     
     - parameter queue: The dispatch queue to use.
     - parameter responseSerializer: The the serializer.
     - returns: The observable of `T.SerializedObject` for the created download request.
     */
    
    func resultWithBodyInError<T: DataResponseSerializerProtocol>(queue: DispatchQueue = .main, responseSerializer: T) -> Observable<T.SerializedObject> {
            return Observable.create { observer in
                let dataRequest = self.validateSuccessfulResponseExt()
                    .response(queue: queue, responseSerializer: responseSerializer) { (packedResponse) -> Void in
                        Logger.logResponse(packedResponse: packedResponse)
                        
                        switch packedResponse.result {
                        case let .success(result):
                            if let _ = packedResponse.response {
                                observer.on(.next(result))
                                observer.on(.completed)
                            } else {
                                observer.on(.error(RxAlamofireUnknownError))
                            }
                        case let .failure(error):
                            observer.on(.error(RxAlamofireError(error:error,packedResponse:packedResponse)))
                        }
                }
                return Disposables.create {
                    dataRequest.cancel()
                }
            }
    }
    
    /// - returns: A validated request based on the status code
    private func validateSuccessfulResponseExt() -> DataRequest { return self.base.validate(statusCode: 200 ..< 300) }
}

class RxAlamofireError<T>:Error {
    var localizedDescription: String { get{_localizedDescription ?? error?.localizedDescription ?? ""} }
    
    private var _localizedDescription:String?
    private var error:Error?
    
    let statusCode:Int?
    let body:String?
    let date:String?
    
    init(error:Error,packedResponse:DataResponse<T,AFError>) {
        self.statusCode = packedResponse.response?.statusCode
        self.error = error
        self.date = packedResponse.response?.allHeaderFields["Date"] as? String
        if let data = packedResponse.data,
            let string = String(data: data, encoding: String.Encoding.utf8){
            body = string
        } else { body = nil }
    }
    
    init(localizedDescription:String) {
        self._localizedDescription = localizedDescription
        statusCode = nil
        body = nil
        date = nil
    }
}

extension String {
    /// check is Blank
    ///
    /// - Returns: is Blank
    var isBlank:Bool{
        let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
