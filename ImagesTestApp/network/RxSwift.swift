//
//  RxSwift.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import RxSwift

extension ObservableType{
    /// observe on background
    /// - Returns: Observable that observe on background
    public func observeOnBackground() -> Observable<Element> {
        return observe(on: ConcurrentDispatchQueueScheduler(qos: .background))
    }
    
    /// subscribe on Main
    /// - Returns: Observable subscribe on Main
    public func subscribeMain(onNext: @escaping ((Self.Element) -> Swift.Void), onError:@escaping ((Error) -> Void)) -> Disposable {
        return observe(on: MainScheduler.instance)
            .subscribe(onNext: onNext, onError: onError)
    }
}

