//
//  ResponseFile.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 13/1/22.
//

import Foundation
import ObjectMapper

//public class ResponseImageModel: Mappable {
//    var id: Int?
//    var author: String?
//    var width: Int?
//    var height: Int?
//    var url: String?
//    var download_url: String?
//
//    required public init?(map: Map) {}
//
//    public func mapping(map: Map) {
//        id <- map["id"]
//        author <- map["author"]
//        width <- map["width"]
//        height <- map["height"]
//        url <- map["url"]
//        download_url <- map["download_url"]
//    }
//}

public struct ResponseImageModel: Codable, Equatable {
    var id: String?
    var author: String?
    var width: Int?
    var height: Int?
    var url: String?
    var download_url: String?
}

