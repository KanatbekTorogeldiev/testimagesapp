//
//  RealmModels.swift
//  ImagesTestApp
//
//  Created by Kanat Torogeldiev on 14/1/22.
//

import Foundation
import RealmSwift

class ImagesRealmModel: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var author: String = ""
    @objc dynamic var width: Int = 0
    @objc dynamic var height: Int = 0
    @objc dynamic var url: String = ""
    @objc dynamic var download_url: String = ""
}
